from .views import *
from django.shortcuts import render
from django.urls import path

app_name = 'lab6'
urlpatterns = [
    path('', status, name='status'),
    path('save_status/', save_status, name='save_status'),
]
